############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File    : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#!/bin/sh
#editeur.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************

global sysFont tableaumots tableauindices tableauindices2 listexo plateforme tabaide tablongchamp tablistevariable tabstartdirect categorie initrep tablecture_mot tablecture_mot_cache dirty


set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source path.tcl
source menus.tcl
source fonts.tcl
source msg.tcl



inithome

set bgn #ffff80
set bgl #ff80c0
set categorie ""
set dirty 0
set initrep [file join $Home textes]
set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\175\0401\0400\175\040\173\173[mc {Flash}]\175\040\173[mc {Flash}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173\173[mc {Rapido}]\175\040\173[mc {Rapido}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\175\0401\0401\175\040\173Dictee\040[mc {Dict�e}]\040\173[mc {Compl�te et appuie sur le bouton pouce.}]\175\0401\0401\175
#set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\0401\0401\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\0401\0401\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\0401\0401\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\0401\0401\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\0401\0401\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 3}]\175\040\173[mc {Exercice 3}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 4}]\175\040\173[mc {Exercice 4}]\0401\0401\175\0401\0400\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\0401\0401\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\0401\0401\175\0401\0401\175\040\173Dictee\040[mc {Dict�e}]\040\173[mc {Compl�te et appuie sur le bouton pouce.}]\175\0401\0401\175

foreach i {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 bisque3 bisque4 ponctuation1 ponctuation2 dictee} {
set tabaide($i) 2
set tablongchamp($i) 1
set tablistevariable($i) 1
set tabstartdirect($i) 1
set tablecture_mot($i) 0
set tablecture_mot_cache($i) 0
}


. configure -background black -width 640 -height 480
wm geometry . +52+0
frame .menu -height 40
pack .menu -side top -fill both
frame .bframe -height 40
pack .bframe -side bottom -fill both

label .bframe.consigne
pack .bframe.consigne

text .text -yscrollcommand ".scroll set" -setgrid true -width 48 -height 12 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

menu_frame .menu .text
menubutton .menu.reglages -text [mc {Reglages}] -pady 7
bind .menu.reglages <1> "global dirty; set dirty 1; source reglagespe.tcl"
pack .menu.reglages -side left

focus .text

bind .text <ButtonRelease-1> "highlight"
bind .text <ButtonRelease-3> "lowlight"
bind .menu <Destroy> "testrecord .text"
bind .text <KeyRelease> "global dirty; set dirty 1"

proc tague {} {
global sysFont tableaumots str dirty
set dirty 1
    if {$str !="" && [string map {" " ""} [.bframe.reponse get]] != ""} {
    set tableaumots($str) [string map {\" \042} [.bframe.reponse get]]
    } else {
    catch {unset tableaumots($str)}
    }
}

proc tagueindice {} {
global sysFont tableauindices str dirty
set dirty 1
    if {$str !="" && [string map {" " ""} [.bframe.repindice get]] != ""} {
    set tableauindices($str) [string map {\" \\042} [.bframe.repindice get]]
    } else {
    catch {unset tableauindices($str)}
    }
}

proc tagueindice2 {} {
global sysFont tableauindices2 str dirty
set dirty 1
    if {$str !="" && [string map {" " ""} [.bframe.reponse get]] != ""} {
    set tableauindices2($str) [string map {\" \\042} [.bframe.reponse get]]
    } else {
    catch {unset tableauindices2($str)}
    }
}

proc lowlight {} {
variable marque 
#variable substexte
global tableauindices tableaumots tableauindices2 dirty
set dirty 1
    if {$marque=="grey"} {
    if {[catch {set str [.text get sel.first sel.last]}] != 1} {
    catch {unset tableauindices($str)}
    catch {unset tableauindices2($str)}
    catch {unset tableaumots($str)}
    catch {
    destroy .bframe.reponse
    destroy .bframe.consigne
    destroy .bframe.indice
    destroy .bframe.repindice
    #destroy .bframe.checksubst
    #checkbutton .bframe.checksubst -text [mc {Substituer au texte}] -variable substexte -relief flat -activebackground grey
    #grid .bframe.checksubst -row 3 -column 2
    }
    }
    }
    if {$marque!="rien"} {
    catch {.text tag remove $marque sel.first sel.last}
    .text tag configure $marque -background $marque
    }
}

proc highlight {} {
global sysFont tableaumots str tableauindices tableauindices2 dirty
variable marque
variable substexte
set dirty 1
    if {$marque!="rien"} {
    catch {.text tag add $marque sel.first sel.last}
    .text tag configure $marque -background $marque

        if {$marque=="grey"} {
            if {[catch {set str [.text get sel.first sel.last]}]} {
            catch {
		destroy .bframe.reponse
		destroy .bframe.consigne
		destroy .bframe.indice
		destroy .bframe.repindice
            #destroy .bframe.checksubst
		}
            return
            }
            catch {
		destroy .bframe.reponse
		destroy .bframe.consigne
		destroy .bframe.indice
		destroy .bframe.repindice
            #destroy .bframe.checksubst
		}
    	#checkbutton .bframe.checksubst -text [mc {Substituer au texte}] -variable substexte -relief flat -activebackground grey
    	#grid .bframe.checksubst -row 3 -column 2

	label .bframe.consigne -text [mc {Mot de remplacement}]
	grid .bframe.consigne -row 0 -column 0

	entry .bframe.reponse
	grid  .bframe.reponse -row 1 -column 0
	bind .bframe.reponse <KeyRelease> "tague"
	.bframe.reponse delete 0 end
	label .bframe.indice -text [mc {Indice}]
	grid .bframe.indice -row 0 -column 1 -sticky w
	entry .bframe.repindice -width 35
	grid .bframe.repindice -row 1 -column 1 -sticky w
	bind .bframe.repindice <KeyRelease> "tagueindice"
	.bframe.repindice delete 0 end
	set str [.text get sel.first sel.last]
	catch {.bframe.repindice insert end $tableauindices($str)}
	catch {.bframe.reponse insert end $tableaumots($str)}
      }

        if {$marque=="purple"} {
            if {[catch {set str [.text get sel.first sel.last]}]} {
            destroy .bframe.reponse
            destroy .bframe.consigne
            return
            }
            catch {
            destroy .bframe.reponse
            destroy .bframe.consigne
            label .bframe.consigne -text "Indice :  "
            grid .bframe.consigne -row 1 -column 0
            entry .bframe.reponse 
            grid .bframe.reponse -row 1 -column 1 -sticky w
            bind .bframe.reponse <KeyRelease> "tagueindice2"
            .bframe.reponse delete 0 end
            set str [.text get sel.first sel.last]
            .bframe.reponse insert end $tableauindices2($str)
            }
        }
   

    }
}


proc testrecord {t} {
global dirty
if {$dirty== 1} {
set dirty 0
set answer [tk_messageBox -message [mc {Enregistrer les modifications?}] -type yesno -icon info]
if {$answer == "yes"} {
sauve $t
}
}
}








