Le cousin S�bastien (1).
Ah ! Comme il �tait dr�le, le petit cousin S�bastien ! La campagne, vraiment, il n'y connaissait rien de rien, mais ce n'�tait pas de sa faute : il vivait � la ville. Sa fa�on de s'�tonner de tout faisait pouffer de rire Marie-Blanche et Coraline.
- Oh! s'�criait-il, par exemple. Venez voir, venez voir! 
Les deux petites filles accouraient en se bousculant.