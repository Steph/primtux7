Il y avait un champ. 

Il y avait une fois un champ o� il ne poussait rien. La pluie le mouillait, le soleil le chaufffait, et il n'y poussait rien. 
La pluie, qui faisait tout ce qu'elle pouvait, se dit : � C'est la faute du soleil qui ne le chauffe pas bien, s'il n'y pousse rien. �
Et elle s'attarda, le soleil arriva. 
- Soleil, lui dit la pluie, avant de partir, je voulais te dire : Chauffe le champ, moi je l'ai bien mouill� maintenant ; chaque fois que je reviens, il n'y pousse rien. 
- Je chauffe, je chauffe, r�pondit le soleil, et je croyais que c'�tait toi qui ne le mouillait pas bien. 
Et souvent ainsi, la pluie mouilla le champ, le soleil le chauffa et il n'y poussait rien. 
Mais un homme vint un jour : � Tiens ! pensa-t-il, voil� un champ o� il ne pousse rien !... � Et il se mit � l'ouvrage. 
Il remua la terre, en sortit des cailloux, creusa de longs sillons. Sous le soleil, il sua, la pluie le trempa, mais il continua... Il avait du courage. 
Puis il sema le grain. Et la pluie p�n�tra dans la terre remu�e, et le grain germa. 
Et de toutes petites tiges point�rent au-dessus de la terre. Le soleil les chauffa. 
Elles pouss�rent. 
Le soleil les dora. 
Et ce furent de grands bl�s d'or que l'�t� l'homme r�colta. 


