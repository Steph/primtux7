Max.

� Max n'est pas avec vous, ce matin ?
- Si, si, ma�tresse... Comme on �tait en retard, c'est lui qui conduit Nanouche � la maternelle, il va revenir.�
C'est vrai ; Max revient, jette un coup d'oeil dans la cour des grands, s'assure que Liline et Fanfan sont bien l�, et repart � travers la ville, jusqu'� la maison... Ce soir, il sera l�, bein � l'heure, assis au milieu des mamans, les oreilles dress�es, attendant sa petite troupe.
� Il faudra que je lui apporte un bon os et un sucre, pense la ma�tresse en regardant Max, le bon chien noir, s'�loigner en trottinant.�
