L'histoire de Lisa (3).
- Comme Madame Brunet, dit J�r�me. Elle habite aussi une petite maison. On va la d�molir et elle sera oblig�e d'habiter un appartement. Mais elle ne veut pas. 
- Si, elle veut bien, dit Lisa. Elle a dit que ce sera bien pour ses vieilles jambes. Elle aura un ascenseur et elle aura une salle de bains.
- Non, elle ne veut pas ! dit J�r�me. Elle a dit qu'elle ne veut pas habiter une pi�ce minuscule et passer son temps dans une baignoire.