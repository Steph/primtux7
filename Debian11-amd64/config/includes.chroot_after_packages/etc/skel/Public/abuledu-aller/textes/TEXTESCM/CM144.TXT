Poil de Carotte.
Il cherche un endroit o� il puisse apprendre � nager. Enfin, il se d�cide. Il s'assied par terre et t�te l'eau d'un orteil que ses chaussures trop �troites ont �cras�. En m�me temps, il se frotte l'estomac qui, peut-�tre, n'a pas fini de dig�rer. Enfin, il se laisse glisser le long des racines.
Jules Renard.