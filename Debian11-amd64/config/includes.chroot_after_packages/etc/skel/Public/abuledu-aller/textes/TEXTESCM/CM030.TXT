Le secret de ma�tre Cornille (16).
A ce moment, les �nes arrivent sur la plate-forme, et nous nous mettons tous � crier bien fort, comme au beau temps des meuniers : 
� Oh� ! du moulin !... Oh� ! ma�tre Cornille ! �
Et voil� les sacs qui s'entassent devant la porte et le beau grain roux qui se r�pand par terre, de tous c�t�s... 
Ma�tre Cornille ouvrait de grands yeux. Il avait pris du bl� dans le creux de sa vieille main et il disait, riant et pleurant � la fois : 
� C'est du bl� !... Seigneur Dieu !... Du bon bl� ! Laissez-moi que je le regarde. �
Alphonse Daudet.