Le chat. 
Un apr�s-midi, la pluie tombait, et, pendant que les parents �taient occup�s, les petites se mirent � jouer autour de la table. 
- Vous ne devriez pas jouer � �a, dit Alphone, leur chat. Ce qui va arriver, c'est que vous allez encore casser quelque chose. Et les parents vont crier. 
- Si on t'�coutait, r�pondit Delphine, on ne jouerait jamais � rien. Et les petites se mirent � courir. 
Au milieu de la table, il y avait un plat en fa�ence qui �tait dans la maison depuis cent ans et auquel les parents tenaient beaucoup. En courant, Delphine et Marinette empoign�rent le pied de la table. Le plat en fa�ence glissa doucement et tomba sur le carrelage o� il fit plusieurs morceaux. 
- Alphonse, il y a le plat en fa�ence qui vient de se casser. Qu'est-ce qu'on va faire? 
- Ramassez les d�bris et allez les jeter dans un foss�. Les parents ne s'apercevront peut-�tre de rien. Mais non, il est trop tard. Les voil� qui rentrent. En voyant les morceaux du plat en fa�ence, les parents furent si en col�re qu'ils se mirent � sauter comme des puces au travers de la cuisine. 
- Malheureuses ! criaient-ils, un plat qui �tait dans la famille depuis cent ans ! Demain, vous partirez chez la tante M�lina. La tante M�lina �tait une tr�s m�chante femme. 
- Pauvres enfants, soupira le chat. Pour un vieux plat d�j� �br�ch�, c'est �tre bien s�v�re.
