TROTT. 

Ce soir, Trott a voulu aller trouver sa maman, et s'asseoir, comme il fait d'habitude, sur son petit fauteuil. Et voici qu'� sa place il y avait la petite soeur dans son berceau. Et sa maman �tait si occup�e � lui faire des mines qu'elle a � peine donn� � Trott un petit baiser tr�s leste. Alors Trott a eu tr�s froid dans le coeur et est all� s'assoeir, tout seul, pr�s de la fen�tre. 
Que faire ? Dire qu'autrefois on l'aimait tant, tant ! Et quand il a �t� malade, c'est comme si on l'avait aim� plus encore. Est-ce que si Trott �tait malade maintenant, peut �tre que... 
C'est une id�e. On a emport� b�b�. Personne ne regarde. D'un bond Trott est debout sur sa chaise. Il appuie ses deux mains sur le dossier et se donne une bonne pouss�e. La chaisse s'�croule avec un fracas �pouvantable, et Trott roule sur le plancher au milieu de la chambre. Maman pousse un cri per�ant. Papa se pr�cipite vers Trott, le rel�ve et se d�p�che de regarder son front. Mais maman veut l'avoir � elle ; elle s'empare de lui, le pose sur ses genoux, le dorlote, le caresse, l'appelle son cher petit maladroit. Trott pleure de joie et de douleur : car il a une belle bosse au front. 


