Epitaphe d'un chien.

Petit museau, petites dents, 
Yeux qui n'�taient point trop ardents,
Mais desquels la prunelle perse 
Imitait la couleur diverse 
Qu'on voit en cet arc pluvieux 
Qui se courbe au travers des cieux ; 
La t�te � la taille pareille, 
Le col grasset, courte l'oreille, 
Et dessous un nez �b�nin 
Un petit mufle l�onin 
Autour duquel �tait plant�e 
Une barbelette argent�e 
Armant d'un petit poil follet 
Son musequin damoiselet ; 
La gorge douillette et mignonne, 
La queue longue � la guenonne, 
Mouchet�e diversement 
D'un naturel bigarrement : 
Tel fut Belaud la gente b�te 
Qui des pieds jusques � la t�te,
De telle beaut� fut pourvu, 
Que son pareil on n'a point vu.

Joachin Du Bellay.