Le soleil et le vent.
Un jour, le soleil et le vent se disput�rent. 
- Je suis plus fort que toi, d�clara le vent. 
- Non, r�pondit l'autre, c'est moi qui suis le plus fort. 
Un voyageur vint � passer, v�tu d'un gros manteau car il faisait froid.
- Tu vois cet homme, dit le vent ; celui qui parviendra � lui �ter son manteau aura gagn�. 
- Bien, r�pliqua le soleil. 
Le vent se mit � souffler, � souffler ; l'homme serra son manteau contre lui.
- Quelle temp�te! s'�cria-t-il.
Puis le soleil se mit � briller.
- Comme le temps a chang� ! fit le voyageur en �tant son manteau.
- Gagn�, dit le soleil.
- Attends, r�pondit le vent.
Et il se mit � souffler, souffler.
- Le temps est compl�tement d�traqu� ! et l'homme remit son v�tement.
- Tu as vu ! s'�cria le vent, tu as oblig� le voyageur � enlever son manteau et moi � le lui faire remettre. Nous sommes aussi forts l'un que l'autre.
Et jamais plus ils ne se disput�rent.