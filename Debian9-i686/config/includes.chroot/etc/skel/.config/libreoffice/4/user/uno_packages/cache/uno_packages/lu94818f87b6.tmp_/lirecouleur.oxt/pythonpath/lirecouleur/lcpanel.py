#!/usr/bin/env python
# -*- coding: UTF-8 -*-

###################################################################################
# LireCouleur - outils d'aide à la lecture
#
# voir http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 5.0.0
# @since 2019
# https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=87057
#
# GNU General Public Licence (GPL) version 3
#
# LireCouleur is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
# LireCouleur is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# LireCouleur; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
###################################################################################
# this is a modified version of http://aoo-extensions.sourceforge.net/en/project/watchingwindow extension
import unohelper
import uno

import threading
import os

from com.sun.star.awt import (XWindowListener, XActionListener, XMouseListener, XItemListener)
from com.sun.star.lang import XServiceInfo
from com.sun.star.ui import XUIElement, XToolPanel

from com.sun.star.awt import ActionEvent

from com.sun.star.awt.PosSize import (X as PS_X, Y as PS_Y, 
    WIDTH as PS_WIDTH, HEIGHT as PS_HEIGHT, SIZE as PS_SIZE, POSSIZE as PS_POSSIZE)

from com.sun.star.ui.UIElementType import TOOLPANEL as UET_TOOLPANEL

from com.sun.star.lang import XComponent
from com.sun.star.beans import PropertyValue
from com.sun.star.beans.PropertyState import DIRECT_VALUE as PS_DIRECT_VALUE

from com.sun.star.awt.MenuItemStyle import CHECKABLE as MIS_CHECKABLE
from com.sun.star.awt.MessageBoxButtons import (
    BUTTONS_OK_CANCEL as MBB_BUTTONS_OK_CANCEL, DEFAULT_BUTTON_CANCEL as MBB_DEFAULT_BUTTON_CANCEL)

EXT_ID = "lire.libre.lirecouleur"
RESOURCE_NAME = "private:resource/toolpanel/lire.libre/lirecouleur"

from .utils import (create_container, get_backgroundcolor, create_uno_service, Settings, LireCouleurLogBox)
    
from .lirecouleurui import (__lirecouleur_phonemes__,__lirecouleur_noir__,__lirecouleur_confusion_lettres__,
        __lirecouleur_consonne_voyelle__,__lirecouleur_couleur_mots__,__lirecouleur_defaut__,__lirecouleur_espace__,
        __lirecouleur_espace_lignes__,__lirecouleur_extra_large__,__lirecouleur_l_muettes__,__lirecouleur_large__,
        __lirecouleur_liaisons_simples__,__lirecouleur_lignes__,__lirecouleur_phon_muet__,__lirecouleur_graphemes_complexes__,
        __lirecouleur_ponctuation__,__lirecouleur_separe_mots__,__lirecouleur_suppr_decos__,__lirecouleur_suppr_syllabes__,
        __lirecouleur_syllabes_soulignees__,__new_lirecouleur_document__,__lirecouleur_alterne_phonemes__,__lirecouleur_appliquer_profil__,
        __lirecouleur_syllabes_dys__,LirecouleurLanceurFonction,getLirecouleurTemplateURL)

class lirecouleurModel(unohelper.Base, XUIElement, XToolPanel, XComponent):
    """ LireCouleur model. """
    
    def __init__(self, ctx, frame, parent):
        self.ctx = ctx
        self.frame = frame
        self.parent = parent
        
        self.view = None
        self.window = None
        try:
            view = lirecouleurView(ctx, self, frame, parent)
            self.view = view
            self.window = view.container
            
            def _focus_back():
                self.frame.getContainerWindow().setFocus()
            
            threading.Timer(0.3, _focus_back).start()
        except:
            pass
    
    # XComponent
    def dispose(self):
        self.ctx = None
        self.frame = None
        self.parent = None
        self.view = None
        self.window = None
    
    def addEventListener(self, ev): pass
    def removeEventListener(self, ev): pass
    
    # XUIElement
    def getRealInterface(self):
        return self
    @property
    def Frame(self):
        return self.frame
    @property
    def ResourceURL(self):
        return RESOURCE_NAME
    @property
    def Type(self):
        return UET_TOOLPANEL
    
    # XToolPanel
    def createAccessible(self, __parent):
        return self.window.getAccessibleContext()
    @property
    def Window(self):
        return self.window
    
    def dispatch(self, cmd, args):
        """ dispatch with arguments. """
        helper = self.ctx.getServiceManager().createInstanceWithContext(
            "com.sun.star.frame.DispatchHelper", self.ctx)
        helper.executeDispatch(self.frame, cmd, "_self", 0, args)
    
    def hidden(self):
        self.data_model.enable_update(False)
    def shown(self):
        self.data_model.enable_update(True)

from com.sun.star.awt.MouseButton import LEFT as MB_LEFT

class lirecouleurView(unohelper.Base, XWindowListener, XActionListener, XMouseListener, XItemListener):
    """ LireCouleur view. """    
    def __init__(self, ctx, model, frame, parent):
        self.model = model
        self.parent = parent
        self.controller = frame.getController()
        self.ctx = ctx
        self._context_menu = None
        self.popupMenuSourceLabel = None
        self.container = None
        self.width = 200
        self.height = 500
        self.LR_MARGIN = 3
        self.TB_MARGIN = 3
        self.BUTTON_SEP = 2
        
        self.settings = Settings(ctx)
        self.FPossibles = self.settings.get("__fonctions_possibles__")
        self.FChoisies = [self.settings.get(fct)[2] for fct in self.FPossibles]
        self.TaillIco = self.settings.get('__taille_icones__')
        self.BUTTON_SZ = self.TaillIco
        
        try:
            self._create_view()
        except:
            LireCouleurLogBox("Echec creation lirecouleurView ")
        parent.addWindowListener(self)
    
    def _create_view(self):
        LR_MARGIN = self.LR_MARGIN
        TB_MARGIN = self.TB_MARGIN
        BUTTON_SEP = self.BUTTON_SEP
        BUTTON_SZ = self.BUTTON_SZ
        WIDTH = self.width
        HEIGHT = self.height
        
        self.fbuttons = []        
        self.container = create_container(self.ctx, self.parent, ("BackgroundColor",), (get_backgroundcolor(self.parent),))

        self.listprof = create_uno_service('com.sun.star.awt.UnoControlListBox')
        list_model = create_uno_service('com.sun.star.awt.UnoControlListBoxModel')
        self.listprof.setModel(list_model)
        self.listprof.setPosSize(LR_MARGIN, TB_MARGIN, 64, 20, PS_POSSIZE)
        list_model.Dropdown = True
        list_model.MultiSelection = False
        self.listprof.addItemListener(self)
        self.container.addControl("list_profils", self.listprof)

        # bouton de validation
        posMaxY = HEIGHT-TB_MARGIN-BUTTON_SZ
        self.edbtn = create_uno_service('com.sun.star.awt.UnoControlButton')
        btn_model = create_uno_service('com.sun.star.awt.UnoControlButtonModel')
        btn_model.setPropertyValues( ('Label',), ("Editer",) )
        self.edbtn.setModel(btn_model)
        self.edbtn.setPosSize(LR_MARGIN, posMaxY, 64, 32, PS_POSSIZE)
        self.container.addControl("edit_lcbar", self.edbtn)
        self.edbtn.setActionCommand("edit_lcbar")
        self.edbtn.addActionListener(self)

        self.plusbtn = create_uno_service('com.sun.star.awt.UnoControlButton')
        btn_model = create_uno_service('com.sun.star.awt.UnoControlButtonModel')
        btn_model.setPropertyValues( ('Label',), ("+",) )
        self.plusbtn.setModel(btn_model)
        self.plusbtn.setPosSize(LR_MARGIN+16+BUTTON_SEP, posMaxY, 32, 32, PS_POSSIZE)
        self.container.addControl("plus_lcbar", self.plusbtn)
        self.plusbtn.setActionCommand("plus_lcbar")
        self.plusbtn.addActionListener(self)

        self.moinsbtn = create_uno_service('com.sun.star.awt.UnoControlButton')
        btn_model = create_uno_service('com.sun.star.awt.UnoControlButtonModel')
        btn_model.setPropertyValues( ('Label',), ("-",) )
        self.moinsbtn.setModel(btn_model)
        self.moinsbtn.setPosSize(LR_MARGIN+64+2*BUTTON_SEP+32, posMaxY, 32, 32, PS_POSSIZE)
        self.container.addControl("moins_lcbar", self.moinsbtn)
        self.moinsbtn.setActionCommand("moins_lcbar")
        self.moinsbtn.addActionListener(self)

        # création des boutons pour les fonctions sélectionnées
        posX = LR_MARGIN
        posY = TB_MARGIN+20+BUTTON_SEP
        for fct in self.FPossibles:
            sting = self.settings.get(fct)
            img = sting[0]
            btn = create_uno_service('com.sun.star.awt.UnoControlImageControl')
            btn_model = create_uno_service('com.sun.star.awt.UnoControlImageControlModel')
            btn_model.setPropertyValues( ('ImageURL','ScaleImage','ScaleMode','HelpText','Name'),
                        ("vnd.sun.star.extension://"+EXT_ID+"/images/"+img,True,1,sting[1],fct,) )
            btn.setModel(btn_model)
            btn.setPosSize(posX, posY, BUTTON_SZ, BUTTON_SZ, PS_POSSIZE)
            self.container.addControl(fct, btn)
            self.fbuttons.append(btn)
            btn.addMouseListener(self)
            posX += (BUTTON_SZ+BUTTON_SEP)
            if (posX+BUTTON_SZ) > WIDTH:
                posY += (BUTTON_SZ+BUTTON_SEP)
                posX = LR_MARGIN
        
    def _update_view(self):
        # mise à jour éventuelle des fonctions accessibles
        desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
        xDocument = desktop.getCurrentComponent()
        xProfil = xDocument.getDocumentProperties().getUserDefinedProperties()
        try:
            lsf = xProfil.getPropertyValue("__selection_fonctions__").split(":")
            self.FChoisies = [0 for fct in self.FPossibles]
            for fct in lsf:
                self.FChoisies[self.FPossibles.index(fct)] = 1
        except:
            pass
        try:
            self.TaillIco = int(xProfil.getPropertyValue("__taille_icones__"))
            self.BUTTON_SZ = self.TaillIco
        except:
            pass

        LR_MARGIN = self.LR_MARGIN
        TB_MARGIN = self.TB_MARGIN
        BUTTON_SEP = self.BUTTON_SEP
        BUTTON_SZ = self.BUTTON_SZ
        HEIGHT = self.height

        # mie à jour des boutons d'après les fonctions sélectionnées
        ps = self.edbtn.getPosSize()
        posMaxY = HEIGHT-TB_MARGIN-ps.Height
        self.edbtn.setPosSize(LR_MARGIN, posMaxY, ps.Width, ps.Height, PS_POSSIZE)
        self.plusbtn.setPosSize(LR_MARGIN+ps.Width+BUTTON_SEP, posMaxY, 32, 32, PS_POSSIZE)
        ps = self.edbtn.getPosSize()
        self.moinsbtn.setPosSize(LR_MARGIN+ps.Width+2*BUTTON_SEP+32, posMaxY, 32, 32, PS_POSSIZE)
        
        self.listprof.setPosSize(LR_MARGIN, TB_MARGIN, self.width-2*LR_MARGIN, 20, PS_POSSIZE)
        ps = self.listprof.getPosSize()

        posX = LR_MARGIN
        posY = TB_MARGIN+ps.Height+BUTTON_SEP
        i = 0
        for btn in self.fbuttons:
            if self.FChoisies[i]:
                btn.setVisible(True)
                btn.setPosSize(posX, posY, BUTTON_SZ, BUTTON_SZ, PS_POSSIZE)
                posX += (BUTTON_SZ+BUTTON_SEP)
                if (posX+BUTTON_SZ) > self.width:
                    posX = LR_MARGIN
                    posY += (BUTTON_SZ+BUTTON_SEP)
            else:
                btn.setVisible(False)
            i += 1

    # XEventListener
    def disposing(self, __ev):
        self.editContainer = None
        self.container = None
        self.model = None
        self._context_menu = None
    
    # XWindowListener
    def windowHidden(self, _ev): pass
    def windowShown(self, _ev):
        self.settings = Settings(self.ctx)
        
        # suppression temporaire du listener
        self.listprof.removeItemListener(self)
        
        # mise à jour de la liste des profils
        try:
            if self.listprof.getItemCount() > 0:
                self.listprof.removeItems(0, self.listprof.getItemCount())
        except:
            pass

        # recup du nom du profil (s'il y en a un) du document courant
        desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
        xDocument = desktop.getCurrentComponent()
        xProfil = xDocument.getDocumentProperties().getUserDefinedProperties()
        profdef = ""
        try:
            a = uno.fileUrlToSystemPath(xProfil.getPropertyValue("LireCouleur_nprofil"))
            profdef = os.path.splitext(os.path.basename(a))[0]
        except:
            pass
        
        # construction de la liste des profils
        isel = 0
        self.listprof.addItem("-- sans profil --", 0)
        for prof in self.settings.get('__profils__'):
            profname = ""
            try:
                a = uno.fileUrlToSystemPath(prof)
                profname = os.path.splitext(os.path.basename(a))[0]
            except:
                pass
            if profdef == profname:
                isel = self.listprof.getItemCount()
            self.listprof.addItem(profname, self.listprof.getItemCount())
        # indique le profil sélectionné
        self.listprof.selectItemPos(isel, True)

        # si le profil n'existe pas (isel=0), on supprime la propriété de nom du profil si elle existe
        if isel == 0 and xProfil.getPropertySetInfo().hasPropertyByName("LireCouleur_nprofil"):
            xProfil.removeProperty("LireCouleur_nprofil")
    
        # remise en place du listener
        self.listprof.addItemListener(self)

    def windowMoved(self, _ev): pass
    def windowResized(self, ev):
        ps = ev.Source.getPosSize()
        self.width = ps.Width
        self.height = ps.Height
        self._update_view()

    # XMouseListener
    def mouseEntered(self, ev):
        btn_model = ev.Source.Model
        btn_model.setPropertyValues( ('BackgroundColor',), (0xffffff,) )
    def mouseExited(self, ev):
        btn_model = ev.Source.Model
        btn_model.setPropertyValues( ('BackgroundColor',), (get_backgroundcolor(self.parent),) )
    def mousePressed(self, ev):
        if ev.Buttons == MB_LEFT and ev.ClickCount == 1:
            img = ""
            try:
                img = ev.Source.Model.ImageURL
            except:
                pass
            if len(img) > 0:
                limg = ["vnd.sun.star.extension://"+EXT_ID+"/images/"+self.settings.get(cmd)[0] for cmd in self.FPossibles]
                try:
                    cmd = self.FPossibles[limg.index(img)]
                    desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
                    xDocument = desktop.getCurrentComponent()
                    if cmd == 'StylePhonemes':
                        LirecouleurLanceurFonction(__lirecouleur_phonemes__, xDocument)
                    elif cmd == 'StyleNoir':
                        __lirecouleur_noir__(xDocument)
                    elif cmd == 'ConfusionLettres':
                        LirecouleurLanceurFonction(__lirecouleur_confusion_lettres__, xDocument, cmd)
                    elif cmd == 'ConsonneVoyelle':
                        LirecouleurLanceurFonction(__lirecouleur_consonne_voyelle__, xDocument, cmd)
                    elif cmd == 'StyleCouleurMots':
                        LirecouleurLanceurFonction(__lirecouleur_couleur_mots__, xDocument, cmd)
                    elif cmd == 'StyleSepareMots':
                        LirecouleurLanceurFonction(__lirecouleur_separe_mots__, xDocument, cmd)
                    elif cmd == 'StyleDefaut':
                        __lirecouleur_defaut__(xDocument)
                    elif cmd == 'StyleEspace':
                        LirecouleurLanceurFonction(__lirecouleur_espace__, xDocument, cmd)
                    elif cmd == 'StylePara':
                        LirecouleurLanceurFonction(__lirecouleur_espace_lignes__, xDocument, cmd)
                    elif cmd == 'StyleExtraLarge':
                        LirecouleurLanceurFonction(__lirecouleur_extra_large__, xDocument, cmd)
                    elif cmd == 'StyleLMuettes':
                        LirecouleurLanceurFonction(__lirecouleur_l_muettes__, xDocument, cmd)
                    elif cmd == 'StyleLarge':
                        LirecouleurLanceurFonction(__lirecouleur_large__, xDocument, cmd)
                    elif cmd == 'StyleLiaisons':
                        LirecouleurLanceurFonction(__lirecouleur_liaisons_simples__, xDocument, cmd)
                    elif cmd == 'StyleLignesAlternees':
                        LirecouleurLanceurFonction(__lirecouleur_lignes__, xDocument, cmd)
                    elif cmd == 'StylePhonMuet':
                        __lirecouleur_phon_muet__(xDocument)
                    elif cmd == 'StyleGraphemesComplexes':
                        LirecouleurLanceurFonction(__lirecouleur_graphemes_complexes__, xDocument, cmd)
                    elif cmd == 'StylePonctuation':
                        LirecouleurLanceurFonction(__lirecouleur_ponctuation__, xDocument, cmd)
                    elif cmd == 'StyleSepareMots':
                        LirecouleurLanceurFonction(__lirecouleur_separe_mots__, xDocument, cmd)
                    elif cmd == 'SupprimerDecos':
                        __lirecouleur_suppr_decos__(xDocument)
                    elif cmd == 'SupprimerSyllabes':
                        __lirecouleur_suppr_syllabes__(xDocument)
                    elif cmd == 'StyleSyllabes':
                        LirecouleurLanceurFonction(__lirecouleur_syllabes_soulignees__, xDocument, cmd)
                    elif cmd == 'StyleSyllDys':
                        LirecouleurLanceurFonction(__lirecouleur_syllabes_dys__, xDocument, cmd)
                    elif cmd == 'NewLireCouleurDocument':
                        __new_lirecouleur_document__(xDocument, self.ctx)
                    elif cmd == 'StylePhonemesAlternes':
                        LirecouleurLanceurFonction(__lirecouleur_alterne_phonemes__, xDocument, cmd)
                    elif cmd == 'ApplicProfil':
                        __lirecouleur_appliquer_profil__(xDocument, self.ctx)
                except:
                    pass

    def mouseReleased(self, ev): pass
    
    def itemStateChanged(self, _ev):
        # normalement, c'est une modification de profil
        desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
        xDocument = desktop.getCurrentComponent()
        ipr = self.listprof.getSelectedItemPos()
        if ipr > 0:
            profilUrl = self.settings.get('__profils__')[ipr-1]
            
            # sélection de tout le texte
            xTextViewCursor = xDocument.getCurrentController().getViewCursor()
            xTextViewCursor.gotoEnd(False)
            xTextViewCursor.gotoStart(True)
    
            # mise à jour du document avec le profil choisi
            __lirecouleur_appliquer_profil__(xDocument, self.ctx, profilUrl)
            
            # mise à jour de la vue
            self._update_view()
        else:
            # suppression de toutes les informations de profil conservées dans le document
            xProfil = xDocument.getDocumentProperties().getUserDefinedProperties()
            for prop in ["LireCouleur_profil", "LireCouleur_nprofil", "__alternate__", "__detection_phonemes__", "__locale__", "__point__",
                         "__selection_lettres__", "__selection_phonemes__",  "__syllo__","__selection_fonctions__", "__taille_icones__"]:
                try:
                    if xProfil.getPropertySetInfo().hasPropertyByName(prop):
                        xProfil.removeProperty(prop)
                except:
                    pass

    # XActionListener
    def actionPerformed(self, ev):
        cmd = ev.ActionCommand
        if cmd == 'edit_lcbar':
            self.editLCBar()
        elif cmd == 'plus_lcbar':
            if self.BUTTON_SZ < 64:
                # éviter d'avoir des icones trop grosses
                self.BUTTON_SZ += 2
                self.settings.setValue("__taille_icones__", self.BUTTON_SZ)
        elif cmd == 'moins_lcbar':
            if self.BUTTON_SZ > 16:
                # éviter d'avoir des icones trop petites
                self.BUTTON_SZ -= 2
                self.settings.setValue("__taille_icones__", self.BUTTON_SZ)
        elif cmd == 'valid':
            i = 0
            lsf = []
            for checker in self.checkList:
                sting = self.settings.get(self.FPossibles[i])
                self.FChoisies[i] = checker.State
                if checker.State:
                    lsf.append(self.FPossibles[i])
                sting[2] = str(self.FChoisies[i])
                self.settings.setValue(self.FPossibles[i], ':'.join(sting))
                i += 1
            self.editContainer.endExecute()
            self.settings.setValue("__selection_fonctions__", lsf)
            
        # mise à jour éventuelle du profil de fonctions
        desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
        xDocument = desktop.getCurrentComponent()
        xProfil = xDocument.getDocumentProperties().getUserDefinedProperties()
        hasProfil = False
        # vérification préalable : au moins une propriété a déjà été définie et indique que le document comprote un profil LireCouleur
        for prop in ["__alternate__", "__detection_phonemes__", "__locale__", "__point__", "__selection_lettres__","__selection_phonemes__", 
                     "__syllo__","__selection_fonctions__", "__taille_icones__","LireCouleur_profil","LireCouleur_nprofil"]:
            try:
                if xProfil.getPropertySetInfo().hasPropertyByName(prop):
                    hasProfil = True
            except:
                pass

        if hasProfil:
            try:
                if xProfil.getPropertySetInfo().hasPropertyByName("__taille_icones__"):
                    xProfil.removeProperty("__taille_icones__")
                xProfil.addProperty("__taille_icones__", MAYBEVOID + REMOVEABLE + MAYBEDEFAULT, self.settings.getStrValue("__taille_icones__"))
            except:
                pass
            try:
                if xProfil.getPropertySetInfo().hasPropertyByName("__selection_fonctions__"):
                    xProfil.removeProperty("__selection_fonctions__")
                xProfil.addProperty("__selction_fonctions__", MAYBEVOID + REMOVEABLE + MAYBEDEFAULT, self.settings.getStrValue("__selection_fonctions__"))
            except:
                pass

        # mise à jour de la vue
        self._update_view()
        
    def editLCBar(self):
        smgr = self.ctx.ServiceManager
        dialogModel = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialogModel", self.ctx)
        
        dialogModel.PositionX = 100
        dialogModel.PositionY = 50
        dialogModel.Width = 180
        dialogModel.Height = 284
        dialogModel.Title = "Choisir les fonctions LireCouleur"

        validBtn = dialogModel.createInstance('com.sun.star.awt.UnoControlButtonModel')
        validBtn.Label = "Valider"
        validBtn.Width = 50
        validBtn.Height = 20
        validBtn.PositionX = (dialogModel.Width-validBtn.Width)/2
        validBtn.PositionY = dialogModel.Height-validBtn.Height-2
        validBtn.Name = "valid"
        dialogModel.insertByName(validBtn.Name, validBtn)
        
        sz_button = 24
        nb_col = int(len(self.FPossibles)*(sz_button+2) / validBtn.PositionY + 1)
        sz_col = dialogModel.Width/nb_col
        posX = self.LR_MARGIN
        posY = self.TB_MARGIN
        self.checkList = []
        i = 0
        for fct in self.FPossibles:
            try:
                img = self.settings.get(fct)[0]
                labelEsp = dialogModel.createInstance("com.sun.star.awt.UnoControlImageControlModel")
                labelEsp.PositionX = posX
                labelEsp.PositionY = posY
                labelEsp.Width  = sz_button
                labelEsp.Height = sz_button
                labelEsp.HelpText = self.settings.get(fct)[1]
                labelEsp.Name = fct+'img'
                labelEsp.ImageURL = "vnd.sun.star.extension://"+EXT_ID+"/images/"+img
                labelEsp.ScaleImage = True
                labelEsp.ScaleMode = 1
                dialogModel.insertByName(labelEsp.Name, labelEsp)

                checkBP = dialogModel.createInstance("com.sun.star.awt.UnoControlCheckBoxModel")
                checkBP.PositionX = posX+sz_button
                checkBP.PositionY = posY
                checkBP.Width  = 10
                checkBP.Height = 10
                checkBP.Name = fct
                checkBP.State = self.FChoisies[i]
                checkBP.Label = ''
                dialogModel.insertByName(checkBP.Name, checkBP)
                self.checkList.append(checkBP)

                posY += (sz_button + 2)
                if (posY+sz_button) > validBtn.PositionY:
                    posX += sz_col
                    posY = self.TB_MARGIN
                    
                i += 1
            except:
                pass

        # create the dialog control and set the model
        controlContainer = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialog", self.ctx)
        controlContainer.setModel(dialogModel)
        self.editContainer = controlContainer
        
        validCtrl = controlContainer.getControl(validBtn.Name)
        validCtrl.setActionCommand(validBtn.Name)
        validCtrl.addActionListener(self)

        # create a peer
        toolkit = smgr.createInstanceWithContext("com.sun.star.awt.ExtToolkit", self.ctx)

        controlContainer.setVisible(False);
        controlContainer.createPeer(toolkit, None);

        # execute it
        controlContainer.execute()

        # dispose the dialog
        controlContainer.dispose()
