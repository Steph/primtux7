Les instruments � vent. 
Il existe un tr�s grand nombre d'instruments de formes diff�rentes et qui ne fonctionnent pas de la m�me mani�re. 
On ne joue pas de la m�me fa�on du tambour et de la guitare. 
Certains instruments poss�dent des cordes qu'on fait vibrer, d'autres poss�dent des tubes dans lesquels on souffle. Sur d'autres enfin, on frappe pour obtenir un son et produire un rythme. 
On appelle instruments � cordes ceux qui ont des cordes qu'on fait vibrer (comme la guitare ou le violon). 
On appelle instruments � vent ceux qui ont un (ou des tubes) dans lesquels on souffle l'air (comme la fl�te ou la trompette). 
On appelle instruments � percussion, ceux sur lesquels on frappe avec la main ou avec des baguettes (comme le tambour, la grosse caisse, le tambourin ou les castagnettes. 

