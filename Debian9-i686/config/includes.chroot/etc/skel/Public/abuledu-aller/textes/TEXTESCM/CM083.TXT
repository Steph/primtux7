La vieille maison (12).
Le soir, comme c'�tait l'usage dans le pays, une voiture tendue de noir s'arr�ta devant la porte ; on y pla�a un cercueil, qu'on devait porter bien loin, pour le mettre dans un caveau de famille. La voiture se mit en marche ; personne ne suivait que le vieux domestique ; tous les amis du vieux monsieur �taient morts avant lui. 
Le petit gar�on pleurait, et il envoyait de la main des baisers d'adieu au cercueil. 
Andersen.