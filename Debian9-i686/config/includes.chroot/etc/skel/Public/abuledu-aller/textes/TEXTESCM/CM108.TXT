Le soleil et le vent (1).
Un jour, le soleil et le vent se disput�rent. 
- Je suis plus fort que toi, d�clara le vent. 
- Non, r�pondit l'autre, c'est moi qui suis le plus fort. 
Un voyageur vint � passer, v�tu d'un gros manteau car il faisait froid.
- Tu vois cet homme, dit le vent ; celui qui parviendra � lui �ter son manteau aura gagn�. 
- Bien, r�pliqua le soleil. 
Le vent se mit � souffler, � souffler ; l'homme serra son manteau contre lui.
- Quelle temp�te! s'�cria-t-il.