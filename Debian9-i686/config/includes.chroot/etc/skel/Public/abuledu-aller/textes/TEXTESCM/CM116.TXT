Le chat et les souris (2).
Alors, un souriceau se leva et d�clara :
- Mon plan est tr�s simple : si le chat portait un grelot autour du cou, nous serions tenus au courant de chacun de ses mouvements, ce qui nous laisserait le temps de nous enfuir.
Et il s'assit, tr�s content de lui, au milieu des applaudissements. Alors, une vieille souris se leva � son tour, les yeux brillants de malice :
- Un plan excellent, dit-elle, mais... qui attachera le grelot au cou du chat ?