#!/bin/sh
#menus.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 David Lucardi <davidlucardi@aol.com>
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Copyright (C) 2003 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $Id: path.tcl,v 1.10 2007/01/05 12:48:38 david Exp $
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi, Andr� Connes, Eric Seigne
# 
#  *************************************************************************
proc lire {} {
global basedir wordlist sync curt
set wordlist ""
focus .text
	set cur [.text index insert]
	set cur1 "[.text search -backwards -regexp {[\040 \- , . ; : ! ? \']} $cur "insert linestart"]"
	if {$cur1 == ""} {set cur1 "[.text search -backwards "\n"  $cur 1.0]"}
	if {$cur1 != ""} {set cur1 "$cur1 + 1c"} 
	if {$cur1 == ""} {set cur1 "insert linestart"}

	set cur2 "[.text search -regexp {[\040 \- , . ; : ! ? \']} $cur "insert lineend"]"
	if {$cur2 == ""} {set cur2 "[.text search "\n"  $cur end]"}
	if {$cur2 != ""} {set cur2 "$cur2"} 
	if {$cur2 == ""} {set cur2 "insert lineend"}
	set texte [string map {- \040} [.text get $cur1 $cur2]]
	#.text tag remove sel 1.0 end
	.text tag add sel $cur1 $cur2


	      #set texte [.text  get 1.0 "end - 1 chars"]
		#regsub -all {[-]} $texte " " texte


set textet [substphon $texte]
if {$texte !=""} {
speaktexte $textet
}
focus .
}

proc litout {} {
global textelu
regsub -all {[-]} $textelu " " textelu
if {$textelu !=""} {
set textet [substphon $textelu]
speaktexte $textet
}
}

proc substphon {what} {
regsub -all {\.\.\.} $what "." what
regsub -all {[�]} $what "." what
regsub -all {[�]} $what "'" what
regsub -all {[\"]} $what "" what
regsub -all {[-]} $what "" what
regsub -all {[�]} $what "oe" what
return $what
}



proc speaktexte {texte} {
global basedir
# regsub -all {[\{\}\(\)\\\"]} $texte "" texte
# regsub -all {[\240]} $texte " " texte
if {$texte==""} {return}
# catch {exec [file join $basedir cicero bulktalk.py] $texte}
cd $basedir
}

proc setwindowsusername {} {
    global user LogHome
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text [mc {Quel est ton nom?}] -background grey
    pack .utilisateur.frame.labobj -side top 

    listbox .utilisateur.frame.listsce -yscrollcommand ".utilisateur.frame.scrollpage set" -width 15 -height 10
    scrollbar .utilisateur.frame.scrollpage -command ".utilisateur.frame.listsce yview" -width 7
    pack .utilisateur.frame.listsce .utilisateur.frame.scrollpage -side left -fill y -expand 1 -pady 10
    bind .utilisateur.frame.listsce <ButtonRelease-1> "verifnom %x %y"
    foreach i [lsort [glob [file join $LogHome *.log]]] {
    .utilisateur.frame.listsce insert end [string map {.log \040} [file tail $i]]
    }

    button .utilisateur.frame.ok -background gray75 -text [mc {Ok}] -command "interface; destroy .utilisateur"
    pack .utilisateur.frame.ok -side top -pady 70 -padx 10
}

proc verifnom {x y} {
    global env user LogHome filuser baseHome

set ind [.utilisateur.frame.listsce index @$x,$y]
set nom [string trim [.utilisateur.frame.listsce get $ind]]
    if {$nom !=""} {
	set env(USER) $nom
	set filuser $nom.conf

	if {! [file exists [file join $baseHome reglages $filuser]]} {
	set filuser aller.conf
	}

	set user [file join $LogHome $nom.log]
    }
}

proc init {plateforme} {
    global Home basedir baseHome
    if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file copy -force [file join reglages] [file join $Home]
	file copy -force [file join textes] [file join $Home]
    }
    
    switch $plateforme {
	unix {
	    if {![file exists [file join aller log]]} {
		file mkdir [file join aller log]
	    }
	    
	}
	windows {
	    if {![file exists [file join aller log]]} {
		file mkdir [file join aller log]
	    }
	    
    	}
    }
    
}


proc initlog {plateforme ident} {
global LogHome user Home baseHome basedir
    switch $plateforme {
	unix {
	    set LogHome [file join aller log]
	    
	}
	windows {
	    set LogHome [file join aller log]
	}
    }
    if {$ident != ""} {
	set user [file join $LogHome $ident.log]
    } else {
	set user [file join $LogHome aller.log]
    }
if {![file exists [file join $user]]} {
set f [open [file join $user] "w"]
close $f
}
}

proc inithome {} {
    global baseHome basedir Homeconf Home filuser
    variable reperttext
    variable repertconf
    variable demarre
    variable defauttext
    set f [open [file join $baseHome reglages $filuser] "r"]
    set demarre [gets $f]
    set defauttext [gets $f]
    close $f

    set f [open [file join $baseHome reglages repert.conf] "r"]
    set reperttext [gets $f]
    set repertconf [gets $f]
    close $f
    #on synchronise les 2 variables en attendant
#############################################################""""    
   switch $repertconf {
	0 {set Home [file join $baseHome]}
	1 {set Home [file join $basedir]}
    }
    switch $repertconf {
	0 {set Homeconf $baseHome}
	1 {set Homeconf $basedir}
    }
###########################################################"    
}


proc changehome {} {
    global Home basedir baseHome Homeconf filuser
    variable reperttext
    variable repertconf
    variable defauttext
    variable demarre
    set f [open [file join $baseHome reglages $filuser] "r"]
    set demarre [gets $f]
    set defauttext [gets $f]
    set aller [gets $f]
    close $f

    set f [open [file join $baseHome reglages repert.conf] "w"]
    #on synchronise les 2 variables en attendant
    puts $f $repertconf
    puts $f $repertconf
    puts $f $aller
    close $f
#################################################"""
    switch $repertconf {
	0 {set Home $baseHome}
	1 {set Home $basedir}
    }
    switch $repertconf {
	0 {set Homeconf $baseHome}
	1 {set Homeconf $basedir}
   }

###############################################"
}

global basedir Home baseHome iwish Homeconf progaide  listexo disabledfore disabledback abuledu prof filuser

set abuledu 0
set prof 0
set filuser aller.conf

	if {[info tclversion] == "8.4"} {
    	set disabledfore disabledforeground
      set disabledback disabledbackground
	} else {
	set disabledfore fg
      set disabledback bg
	}

set basedir [pwd]
cd $basedir
if {$env(HOME) == "c:\\"} {
    set Home [file join $basedir]
    set Homeconf [file join $basedir]
    
} else {
    set Home [file join $env(HOME) leterrier aller]
    set Homeconf [file join $env(HOME) leterrier aller]
}

set baseHome $basedir

set listexo {{closure Closure Compl�te 1 1} {reconstitution Reconstitution Compl�te 1 1} {phrase {Phrases m�lang�es} {Clique sur une phrase et d�place-l� en cliquant sur les fl�ches} 1 1} {mot {Mots m�lang�s} {Remets les mots dans l'ordre sur les traits roses} 1 1} {faute {Texte � corriger} {Clique sur les mots mal �crits, r��cris-les et appuie sur entr�e.} 1 1} {espace {Phrases sans espaces} {Clique pour s�parer les mots} 1 1} {incomplet {Phrases incompl�tes} {Compl�te} 1 1} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 1} {Exercice 1} 1 0} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 2} {Exercice 2} 1 0} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 3} {Exercice 3} 1 0} {{Exercice suppl�mentaire} {Exercice suppl�mentaire 4} {Exercice 4} 1 0} {ponctuation1 Ponctuation1 {Compl�te avec le signe convenable} 1 1} {ponctuation2 Ponctuation2 {Clique pour r�tablir la ponctuation et compl�te avec le signe convenable} 1 1} {dictee Dictee {Compl�te puis appuie sur le bouton pouce} 1 1}}


switch $tcl_platform(platform) {
    unix {
	set progaide runbrowser
    #if {[lsearch [exec id -nG $env(USER)] "leterrier"] != -1} {set prof 1}
	#if {[file isdirectory [file join /etc abuledu]]} {set abuledu 1}
	set abuledu 1
	set iwish wish
	source sonlinux.tcl
    }
    windows {
	set progaide shellexec.exe
	set iwish wish
	source sonwindows.tcl

    }
}


