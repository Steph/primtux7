Les cinq aveugles et l'�l�phant

Cinq aveugles voulaient savoir � quoi ressemblait un �l�phant.
Le cornac leur permit de toucher l'animal pour s'en faire une id�e.
� L'�l�phant ressemble � un gros serpent �, dit l'aveugle qui avait touch� la trompe.
� Non, il est semblable � un chasse-mouches �, protesta celui qui t�tait l'oreille. 
� Allons donc,  c'est un pilier ! � d�clara celui qui tata�t la jambe. 
Celui qui tenait la queue affirma : � Pas du tout, c'est une corde ! � 
Et celui qui palpait une d�fense se mit � rire :
� Etes-vous sots ! Un �l�phant, cela ressemble � un os ! �...
