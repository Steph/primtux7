Un accident de chemin de fer (2).
Il n'y avait ni morts ni bless�s, quelques contusionn�s seulement, car le train n'avait pas encore repris son �lan, et nous regardions, d�sol�s, la grosse b�te de fer estropi�e, qui ne pourrait plus nous tra�ner et qui barrait la route pour longtemps peut-�tre, car il faudrait sans doute faire venir de Paris un train de secours.
Guy de Maupassant.