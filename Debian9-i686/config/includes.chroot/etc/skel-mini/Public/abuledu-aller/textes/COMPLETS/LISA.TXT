L'histoire de Lisa.
Aujourd'hui, Lisa et J�r�me rentrent ensemble de l'�cole. Lisa a sept ans et J�r�me en a six.
- Je vais te raconter une histoire, dit Lisa.
- Voil�. Il �tait une fois une belle jeune fille. Elle habitait une chaumi�re dans la for�t. 
- Qu'est-ce que c'est, une chaumi�re ?, demande J�r�me.
- Tu sais bien, r�pond Lisa, c'est l'endroit o� habitent les gens dans les histoires. 
- C'est un appartement, alors ?
- Non, pas un appartement. Elle habitait une chaumi�re : c'est comme une petite maison.
- Comme Madame Brunet, dit J�r�me. Elle habite aussi une petite maison. On va la d�molir et elle sera oblig�e d'habiter un appartement. Mais elle ne veut pas. 
- Si, elle veut bien, dit Lisa. Elle a dit que ce sera bien pour ses vieilles jambes. Elle aura un ascenseur et elle aura une salle de bains.
- Non, elle ne veut pas ! dit J�r�me. Elle a dit qu'elle ne veut pas habiter une pi�ce minuscule et passer son temps dans une baignoire.
- Tu l'�coutes, mon histoire, oui ou non ? crie Lisa.
- Oui, dit J�r�me, vas-y.
- Voil�. Alors cette jeune fille habitait une chaumi�re dans la for�t.
- Qu'est-ce que c'est, la for�t ?
- Oh ! Zut ! Il y a des for�ts dans toutes les histoires. 
- Mais qu'est-ce que c'est ? 
- C'est  recouvert de feuilles, dit Lisa.
- Comme la voiture de Monsieur Clerc, au march� ?, dit J�r�me...