Lorsqu' une �toile tombe, c'est qu'une �me monte � Dieu. Elle frotta encore une allumette sur le mur : il se fit une grande lumi�re au milieu de laquelle la grand-m�re se tenait debout, avec un air si doux, si radieux! 
-Grand-m�re, s'�cria la petite, emm�ne-moi. Lorsque l'allumette s'�teindra, je sais que tu n'y seras plus. Tu dispara�tras comme le po�le de fer, comme l'oie r�tie, comme le bel arbre de No�l. Elle frotta promptement le reste du paquet, car elle tenait � garder sa grand-m�re, et les allumettes r�pandirent un �clat plus vif que celui du jour. Jamais la grand-m�re n'avait �t� si grande et si belle. Elle prit la petite fille sur son bras, et toutes les deux s'envol�rent joyeuses au milieu de ce rayonnement, si haut, si haut, qu'il n'y avait plus ni froid, ni faim, ni angoisse ; elles �taient chez Dieu. Mais dans le coin, entre les deux maisons, �tait assise, quand vint la froide matin�e, la petite fille, les joues toutes rouges, le sourire sur la bouche... morte, morte de froid, le dernier soir de l'ann�e. Le jour de l'an se leva sur le petit cadavre assis l� avec les allumettes, dont un paquet avait �t� presque tout br�l�. 
-Elle a voulu se chauffer ! dit quelqu'un. Tout le monde ignora les belles choses qu'elle avait vues, et au milieu de quelle splendeur elle �tait entr�e avec sa vieille grand-m�re dans la nouvelle ann�e. 
(Extrait de: La petite fille et les allumettes d'Andersen)






