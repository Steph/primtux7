Savoir observer (3).
A peine arriv�, il h�le un fiacre, et court comme si le feu �tait � sa maison. Dans la soir�e, vous le retrouverez au th��tre ; il admirera des arbres en carton peint, des fausses moissons, un faux clocher, de faux moissonneurs lui brailleront aux oreilles ; et il dira, tout en frottant ses genoux meurtris par l'esp�ce de bo�te o� il est emprisonn� : 
� Les moissonneurs chantent faux ; mais le d�cor n'est pas laid. �
Alain.