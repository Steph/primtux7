Le moulin �tait grand ouvert... Devant la porte, ma�tre Cornille, assis sur un sac de pl�tre, pleurait, la t�te dans ses mains. Il venait de s'apercevoir, en rentrant, que pendant son absence on avait p�n�tr� chez lui et surpris son triste secret. 
-Pauvre de moi! disait-il. Maintenant, je n'ai plus qu'� mourir... Le moulin est d�shonor�. 
Et il sanglotait � fendre l'�me, appelant son moulin par toutes sortes de noms, lui parlant comme � une personne v�ritable. 
A ce moment, les �nes arrivent sur la plate-forme, d nous nous mettons tous � crier bien fort. comme au beau temps des meuniers: 
-Oh�! du moulin!... Oh�! ma�tre Cornille! 
Et voil� les sacs qui s'entassent devant la porte et le beau grain roux qui se r�pand par terre, de tous c�t�s... 
Ma�tre Cornille ouvrait de grands yeux. Il avait pris du bl� dans le creux de sa vieille main et il disait, riant et pleurant � la fois: 
-C'est du bl�!... Seigneur Dieu!... Du bon bl�! Laissez-moi que je le regarde. 
Puis se tournant vers nous: 
(Extrait de: Le secret de ma�tre Cornille d'Alphonse Daudet)
