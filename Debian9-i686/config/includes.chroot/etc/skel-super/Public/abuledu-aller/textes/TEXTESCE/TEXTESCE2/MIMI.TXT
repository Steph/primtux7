MIMI. 
Tous les soirs, maman demande � Mimi: 
-Qu'as-tu mang� Mimi, � la cantine, � midi ? 
Mais le soir, Mimi ne s'en souvient plus. Alors elle h�site: 
-Heu, je ne sais plus, des p�tes ou du riz, avec du poisson, ou peut-�tre du r�ti... 
Sa maman lui dit: 
-Voyons Mimi, rappelle-toi un peu mieux ! c'est aga�ant, � la fin... Et tous les soirs �a recommence: 
-Qu'as-tu mang� Mimi � midi? 
Alors pour avoir la paix, Mimi se met � inventer. D'abord, elle invente un tout petit peu. Elle dit: - Du poulet et des frites ou bien: - du poisson avec des p�tes. Et comme elle voit que sa maman est contente, elle en invente un peu plus. 
Elle dit: 
 -Du pigeon r�ti aux fanes de radis, ou des patates douces � la sauce crevette. Et sa maman lui r�pond: 
- Hum! Ca devait �tre d�licieux! 
Un soir, comme d'habitude, sa maman, luidemande: 
- Qu'as-tu mang� Mimi � midi? 
Et ce soir-l�, Mimi r�pond: 
-Du poisson chat � la noix de coco! Cette fois, sa maman a l'air tr�s f�ch�. 
-Dis donc Mimi, tu ne me racontes pas des histoires? 
Et le petit fr�re de Mimi se met � crier. 
-Ouh, la menteuse ! - Ouh, la menteuse. 

