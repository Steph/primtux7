Gargantua
 Le g�ant Gargantua aimait beaucoup voyager. 
Il parcourait souvent le pays, mont� sur son �norme jument.
Une jument de g�ant, grosse comme six �l�phants. 
Un jour, il arriva  � Paris... Pour mieux comtempler la ville, il s'assit sur la cath�drale Notre-Dame.
Ce spectacle amusait beaucoup les Parisiens.
Mais un jour, Gargantua d�crocha les lourdes cloches de la cath�drale.
L�, les Parisiens ne riaient plus du tout : sans les cloches de Notre-Dame, ils ne savaient plus l'heure.
Alors, pour r�cup�rer leurs pr�cieuses cloches, 
les Parisiens servirent � Gargantua un repas... g�ant 
compos� de 300 boeufs cuisin�s en sauce et de 200 moutons r�tis !
Le g�ant fut ravi et rendit les cloches !
