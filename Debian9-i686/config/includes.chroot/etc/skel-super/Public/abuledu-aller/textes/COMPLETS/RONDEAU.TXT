Rondeau.

Le temps a laiss� son manteau
De vent, de froidure et de pluie
Et s'est v�tu de broderie
De soleil riant, clair et beau. 

Il n'y a b�te ni oiseau
Qu'en son jargon ne chante ou crie
� Le temps a laiss� son manteau
De vent, de froidure et de pluie. �

Rivi�re, fontaine et ruisseau
Portent en livr�e jolie
Gouttes d'argent d'orf�vrerie ; 
Chacun s'habille de nouveau. 

Le temps a laiss� son manteau
De vent, de froidure et de pluie. 

Charles d'Orl�ans.