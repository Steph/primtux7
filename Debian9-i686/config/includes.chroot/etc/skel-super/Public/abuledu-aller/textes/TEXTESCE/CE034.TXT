La ch�vre de Monsieur Seguin (5).
- P�ca�re ! Pauvre Renaude !... �a ne fait rien, monsieur Seguin, laissez-moi aller dans la montagne. 
- Bont� divine !... dit M. Seguin ; mais qu'est-ce qu'on leur fait donc � mes ch�vres ? Encore une que le loup va me manger... Eh bien, non... je te sauverai malgr� toi, coquine ! Et de peur que tu ne rompes ta corde, je vais t'enfermer dans l'�table, et tu y resteras toujours. �
Alphonse Daudet.