La ch�vre de Monsieur Seguin (2).
� - Ecoutez, monsieur Seguin, je me languis chez vous, laissez-moi aller dans la montagne. 
- Ah ! mon Dieu ! ... Elle aussi ! �, cria M. Seguin stup�fait, et du coup il laissa tomber son �cuelle ; puis, s'asseyant dans l'herbe � c�t� de sa ch�vre : 
� - Comment, Blanquette, tu veux me quitter ! �
Alphonse Daudet.